G5
----------------
G5 is a hostable online mathematical parser built using [Node](http://nodejs.org) ,[Jison](https://github.com/zaach/jison/blob/master/README.md), [MathJS](https://mathjs.org/) and [ChartJS](https://www.chartjs.org/)


Installation
-------------
Make sure you have npm installed on you system (use **npm -v** to see if it is installed)
Use **npm -install** to retrieve every package used in this project
There is only source code so you have to clone this project



Usage
------
Use **node index.js** to launch server
Then connect to http://localhost:8100 (You can change the port used in index.js)

Once you are connected, it's pretty straigh-forward, simply use the HTML form in the top left hand corner and you're ready to go !


How to use the Doc ?
----------------------------
To open doc, you have to open Visual Studio Code.
You need the "Live Server" extension.
Go to ./docs/ in your repository.
'Right clic' on index.html, then 'Open with Live Server' (or 'Alt' + 'L', 'Alt' + 'O' on WIndows)


Authors
----------------------------
Alexis Vandemoortele, Baptiste Parent, Charles Gérard, Théo Dubois, Thibaut Wartel, Tom Mouquet

Video presentation
----------------------------
https://www.youtube.com/embed/F9E_qcuPDyQ