//@/ts-check

/**
 * @file index.js is the root file for the app
 * @author Alexis Vandemoortele, Baptiste Parent, Charles Gérard, Théo Dubois, Thibaut Wartel, Tom Mouquet
 * @see <a href="https://gitlab.com/BaptISEN/groupe-5">For more informations :)</a>
 */


const port = 8100;

const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);

/*
 * Modules
 */

const { getParsedStack } = require('./src/analyzer');
const { solver } = require('./src/solver');

/**
 * Icon site
 * @type {NodeRequire}
 */
const favicon = require('serve-favicon');
app.use(favicon(__dirname + '/assets/img/favicon.ico'));


/**
 * Location client
 */
app.use(express.static(__dirname + '/assets/'))

/**
 * Adresse des pages
 */
app.get('/', (req, res, next) => res.sendFile(__dirname + '/assets/views/accueil.html'));
app.get('/accueil', (req, res, next) => res.sendFile(__dirname + '/assets/views/accueil.html'));
app.get('/analyseur', (req, res, next) => res.sendFile(__dirname + '/assets/views/index.html'));




/**
 * An operation
 * @typedef {Object} Operation
 * @property {string} operation Entrée originale
 * @property {string} postfixed Opération postfixée
 * @property {string} color Couleur de la courbe
 */


/**
 * A Point
 * @typedef {Object} Point
 * @property {number} x x value
 * @property {number} y y = f(x) value
 */




io.sockets.on('connection', (socket) => {

    console.log("User connected : ", socket.id);
    io.emit("HANDSHAKE", "Done");

    /**
     * Récupère les infos enoyé par le cient, permettant de générer une liste d'objets Point, voir {@link Point}.
     * 
     * Converti l'opération reçu en opération postfixée, voir {@link getParsedStack}.
     * 
     * Vérifie si le parser a détecté une erreur. Si oui, l'envoi au client, voir {@link SERVER_SEND_ERROR}.
     * 
     * Si non, évalue l'opération postfixée sur l'intervalle reçu, voir {@link solver}.
     * 
     * Finalement, envoie la liste d'objets Points au client, voir {@link SERVER_SEND_POINTS}.
     * 
     * @typedef {socket.on} CLIENT_SEND_EXPRESSION
     * @param {Operation} operation Object opération, voir {@link Operation}
     * @param {Array<number>} inter Intervale composé de maxX, maxY, minX, minY
     * @param {number} [nbPoints] Nombre de points affichés sur le graphe
     */
    socket.on('CLIENT_SEND_EXPRESSION', (operation, inter, nbPoints = 1000) => { // expression => argument reçu par socket.emit() envoyé depuis le côté client
        
        console.log(`\nCLIENT_SEND_EXPRESSION\nRequest received: '${operation.operation}'\nInter received: '${inter}'\nNbPoints received: '${nbPoints}'`);
        
        operation.postfixed = getParsedStack(operation.operation);

        console.log(`Postifxed operation: ${operation.postfixed}`);

        /**
         * Test si c'est une erreur avec le tag "ERROR"
         */
        if (operation.postfixed.indexOf("ERROR") == -1) {

            /**
             * Si pas d'erreur, envoie la liste d'objets Points au client.
             */
            let listPoints = solver(operation.postfixed, inter, nbPoints);

            console.log(`List of points size : ${listPoints.length}`);

            socket.emit('SERVER_SEND_POINTS', listPoints, inter, nbPoints, operation);
            console.log(`socket.emit('SERVER_SEND_POINTS')`);
        }
        else {

            /**
             * Si erreur, envoie le string d'erreur au client.
             */
            socket.emit('SERVER_SEND_ERROR', operation.postfixed);
            console.log(`socket.emit('SERVER_SEND_ERROR')`);
        }
    });
});

server.listen(port);

console.log("Server launched !");