/**
 * Analyzer module - See {@tutorial analyzer-tutorial}
 * @module Analyzer
 */

/**
 * @file analyser.js contain the Analysez module, see {@link Analyzer}
 */


/**
 * Jison (équivalent de Bison pour le C/C++)
 * @type {Function}
 */
const Parser = require('jison').Parser




/**
 * Détecte s'il y a des erreurs
 * 
 * @param {error} e Erreur catch dans l'analyseur
 * @returns {string} Erreur potentielle
 */
function errorHandler(e) {

    switch(e.hash.token) {
        case 'NUMBER':
            return("ERROR : Operator probably missing");
        case 'EOF':
            return("ERROR : A number, 'x' or ')' could be missing at the end of your expression")
        case '*':
        case '/':
            return("ERROR : A number is probably missing")
        case null:
            return("ERROR : Your expression contains an undefined token (for instance : using 2x instead of 2*x is not valid)")
        default:
            return("ERROR : Invalid expression");
    }
}



/**
 * Annalyser lexicale et syntaxique.
 * 
 * Envoi l'opération postfixée si tout ok, sinon l'erreur.
 * 
 * @param {string} expression Opération sous forme d'une fonction f(x)=y
 * @returns {string} Opération postfixée ou Erreur
 */
exports.getParsedStack = (expression) => {

    let grammar = {
        
            "lex": {
                "rules": [
                    ["\\s+",                    "/* skip whitespace */"],
                    ["[0-9]+(?:\\.[0-9]+)?\\b", "return 'NUMBER';"],
                    ["\\*",                     "return '*';"],
                    ["\\/",                     "return '/';"],
                    ["-",                       "return '-';"],
                    ["\\+",                     "return '+';"],
                    ["\\^",                     "return '^';"],
                    ["\\(",                     "return '(';"],
                    ["\\)",                     "return ')';"],
                    ["SIN|sin",                 "return 'SIN';"],
                    ["COS|cos",                 "return 'COS';"],
                    ["TAN|tan",                 "return 'TAN';"],
                    ["ASIN|asin",               "return 'ASIN';"],
                    ["ACOS|acos",               "return 'ACOS';"],
                    ["ATAN|atan",               "return 'ATAN';"],
                    ["LN|ln",                   "return 'LOG';"],
                    ["LOG|log",                 "return 'LOG10';"],
                    ["SQRT|sqrt",               "return 'SQRT';"],
                    ["EXP|exp|E|e",             "return 'EXP';"],
                    ["PI|pi\\b",                "return 'PI';"],
                    ["X|x\\b",                  "return 'X';"],
                    ["$",                       "return 'EOF';"]
                ]
            },
        
            "operators": [
                ["left", "+", "-"],
                ["left", "*", "/"],
                ["left", "^"],
                ["left", "SIN", "COS", "TAN", "ASIN", "ACOS", "ATAN"],
                ["left", "LOG", "LOG10"],
                ["left", "SQRT", "EXP"],
            ],
        
            "bnf": {
                "expressions" :[[ "e EOF",   "return $1;"  ]], //"console.log($1); return $1;"
        
                "e" :[[ "e + e",        "$$ = `${$1} ${$3} +`;" ],
                        [ "e - e",        "$$ = `${$3} ${$1} -`;" ],
                        [ "e * e",        "$$ = `${$1} ${$3} *`;" ],
                        [ "e / e",        "$$ = `${$3} ${$1} /`;" ],
                        [ "e ^ e",        "$$ = `${$3} ${$1} ^`;" ],
                        [ "- e",          "$$ = `${$2} n`;" ],
                        [ "+ e",          "$$ = `${$2} p`;" ],
                        [ "( e )",        "$$ = `${$2}`;" ],
                        [ "SIN e",        "$$ = `${$2} SIN`;" ],
                        [ "COS e",        "$$ = `${$2} COS`;" ],
                        [ "TAN e",        "$$ = `${$2} TAN`;" ],
                        [ "ASIN e",       "$$ = `${$2} ASIN`;" ],
                        [ "ACOS e",       "$$ = `${$2} ACOS`;" ],
                        [ "ATAN e",       "$$ = `${$2} ATAN`;" ],
                        [ "LOG e",        "$$ = `${$2} LOG`;"  ],
                        [ "LOG10 e",      "$$ = `${$2} LOG10`;"  ],
                        [ "SQRT e",       "$$ = `${$2} SQRT`;" ],
                        [ "EXP e",        "$$ = `${$2} EXP`;" ],
                        [ "NUMBER",       "$$ = `${yytext}`;" ],
                        [ "PI",           "$$ = `PI`;" ],
                        [ "X",            "$$ = `X`;" ]
                    ]
            }
        };
    
    parser = new Parser(grammar);

    try {
        
        let toReturn = parser.parse(expression);
        return toReturn;
    } 
    catch (error) {

        ErrReason = errorHandler(error);
        console.log(ErrReason);
        return ErrReason;
    }
    


}