$(document).ready(function () {
    // get MathJax output object
    var mjDisplayBox, mjOutBox;
    MathJax.Hub.Queue(function () {
        mjDisplayBox = MathJax.Hub.getAllJax('math-display')[0];
        mjOutBox = MathJax.Hub.getAllJax('math-output')[0];
    });

    // "live update" MathJax when a key is pressed
    $('#basic-url').on('keyup', function (evt) {
        var math = $(this).val();
        $(this).css('color', 'black');
        if (math.length > 0){
            try {
                var tree = MathLex.parse(math),
                    latex = MathLex.render(tree, 'latex');
                MathJax.Hub.Queue(['Text', mjDisplayBox, latex]);
            } catch (err) {
                $(this).css('color', 'red');
            }
        } else {
            // clear display and output boxes if input is empty
            MathJax.Hub.Queue(['Text', mjDisplayBox, '']);
            MathJax.Hub.Queue(['Text', mjOutBox, '    ']);
        }
    });

});