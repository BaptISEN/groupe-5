/**
 * Utilisé pour assets/views/index.html
 * @namespace Settings
 */

/**
 * @file settings.js contain more client script, see {@link Client}
 */


/**
 * Ajoute une fonction dans la bar d'insertion de fonction,
 * selon le bouton du clavier virtuel sur lequel le client appuie.
 * 
 * @memberof Settings
 * @param {number} i ID du bouton sélectionné sur le clavier virtuel
 * @returns {void}
 */
function listenButtons(i) {

    switch (i) {
        case 0:
            //sin
            document.getElementById("basic-url").value += 'sin(';
            document.getElementById("basic-url_virtual").value += 'sin(';
            break;
        case 1:
            //cos
            document.getElementById("basic-url").value += 'cos(';
            document.getElementById("basic-url_virtual").value += 'cos(';
            break;
        case 2:
            //tan
            document.getElementById("basic-url").value += 'tan(';
            document.getElementById("basic-url_virtual").value += 'tan(';
            break;
        case 3:
            //asin
            document.getElementById("basic-url").value += 'asin(';
            document.getElementById("basic-url_virtual").value += 'asin(';
            break;
        case 4:
            //acos
            document.getElementById("basic-url").value += 'acos(';
            document.getElementById("basic-url_virtual").value += 'acos(';
            break;
        case 5:
            //arctan
            document.getElementById("basic-url").value += 'atan(';
            document.getElementById("basic-url_virtual").value += 'atan(';
            break;
        case 6:
            //sqrt
            document.getElementById("basic-url").value += 'sqrt(';
            document.getElementById("basic-url_virtual").value += 'sqrt(';
            break;
        case 7:
            //exp
            document.getElementById("basic-url").value += 'exp(';
            document.getElementById("basic-url_virtual").value += 'exp(';
            break;
        case 8:
            //ln
            document.getElementById("basic-url").value += 'ln(';
            document.getElementById("basic-url_virtual").value += 'ln(';
            break;
        case 9:
            //ln
            document.getElementById("basic-url").value += 'log(';
            document.getElementById("basic-url_virtual").value += 'log(';
            break;
        default:
            break;
    }
}


/**
 * Ajoute une opération dans la bar d'insertion de fonction,
 * selon le bouton du clavier virtuel sur lequel le client appuie.
 * 
 * @memberof Settings
 * @param {number} i ID du bouton sélectionné sur le clavier virtuel
 * @returns {void}
 */
function listenOperators(i) {
    switch (i) {
        case 0:
            document.getElementById("basic-url").value += " + ";
            document.getElementById("basic-url_virtual").value += " + ";
            break;
        case 1:
            document.getElementById("basic-url").value += " - ";
            document.getElementById("basic-url_virtual").value += " - ";
            break;
        case 2:
            document.getElementById("basic-url").value += " * ";
            document.getElementById("basic-url_virtual").value += " * ";
            break;
        case 3:
            document.getElementById("basic-url").value += " / ";
            document.getElementById("basic-url_virtual").value += " / ";
            break;
        case 4:
            document.getElementById("basic-url").value += "(";
            document.getElementById("basic-url_virtual").value += " ( ";
            break;
        case 5:
            document.getElementById("basic-url").value += ")";
            document.getElementById("basic-url_virtual").value += " ) ";
            break;
        case 6:
            document.getElementById("basic-url").value += "^";
            document.getElementById("basic-url_virtual").value += " ^ ";
            break;
        case 7:
            document.getElementById("basic-url").value += "x";
            document.getElementById("basic-url_virtual").value += "x";
            break;
        case 8:
            document.getElementById("basic-url").value += "0";
            document.getElementById("basic-url_virtual").value += "0";
            break;
    }
}

/**
 * Ajoute une opérande dans la bar d'insertion de fonction,
 * selon le bouton du clavier virtuel sur lequel le client appuie.
 * 
 * @memberof Settings
 * @param {number} i ID du bouton sélectionné sur le clavier virtuel
 * @returns {void}
 */
function listenNumbers(i) {
    switch (i) {
        case 0:
            document.getElementById("basic-url").value += "1";
            document.getElementById("basic-url_virtual").value += "1";
            break;
        case 1:
            document.getElementById("basic-url").value += "2";
            document.getElementById("basic-url_virtual").value += "2";
            break;
        case 2:
            document.getElementById("basic-url").value += "3";
            document.getElementById("basic-url_virtual").value += "3";
            break;
        case 3:
            document.getElementById("basic-url").value += "x";
            document.getElementById("basic-url_virtual").value += "x";
            break;
        case 4:
            document.getElementById("basic-url").value += "4";
            document.getElementById("basic-url_virtual").value += "4";
            break;
        case 5:
            document.getElementById("basic-url").value += "5";
            document.getElementById("basic-url_virtual").value += "5";
            break;
        case 6:
            document.getElementById("basic-url").value += "6";
            document.getElementById("basic-url_virtual").value += "6";
            break;
        case 7:
            document.getElementById("basic-url").value += ",";
            document.getElementById("basic-url_virtual").value += ",";
            break;
        case 8:
            document.getElementById("basic-url").value += "7";
            document.getElementById("basic-url_virtual").value += "7";
            break;
        case 9:
            document.getElementById("basic-url").value += "8";
            document.getElementById("basic-url_virtual").value += "8";
            break;
        case 10:
            document.getElementById("basic-url").value += "9";
            document.getElementById("basic-url_virtual").value += "9";
            break;
        case 11:
            document.getElementById("basic-url").value += "0";
            document.getElementById("basic-url_virtual").value += "0";
            break;
    }
}

/*
 * Write fonction
 */
let modalBody = document.getElementById("modal-body");
let fonct = modalBody.getElementsByTagName('a');

for (let i = 0; i < fonct.length; i++) {
    fonct[i].addEventListener("click", (function(arg1) {
        return function() {
            listenButtons(arg1);
        }
    })(i), false);
}

/*
 * Write numbers
 */
let modalNumbers = document.getElementById("numbers");
let numbers = modalNumbers.getElementsByTagName('a');

for (let i = 0; i < numbers.length; i++) {
    numbers[i].addEventListener("click", (function() {
        return function() {
            listenNumbers(i);
        }
    })(i), false);
}

/*
 * Write operators
 */
let modalOp = document.getElementById("operators");
let operators = modalOp.getElementsByTagName('a');

for (let i = 0; i < operators.length; i++) {
    operators[i].addEventListener("click", (function(arg1) {
        return function() {
            listenOperators(arg1);
        }
    })(i), false);
}


/*
 * Partie gérant le copier coller entre le basic-url et le basic-urlVirtual
 */
document.getElementById('virtualKeyboard').addEventListener('click', copyBasicUrlValue);

/**
 * Copie le contenu de basic-url vers basic-urlVirtual
 * @memberof Settings
 * @returns {void}
 */
function copyBasicUrlValue(){
   let temp =  document.getElementById('basic-url').value;
   document.getElementById('basic-url_virtual').value = temp;
}
