//@/ts-check


/**
 * Utilisé pour assets/views/accueil.html
 * @namespace Accueil
 */

/**
 * @file accueil.js contain the accueil script, see {@link Accueil}
 */


 (function(){

    const loc = 'http://localhost:8100'
    const socket = io.connect(loc);
    


    /**
     * Change l'url du navigateur sur localhost/analyseur
     * 
     * @memberof Accueil
     * @returns {void}
     */
    function onButtonAnalyser() {
        window.location.assign(loc+"/analyseur");
    }

    let joinAnalyserButton = document.getElementsByClassName('joinAnalyser');

    for (let button of joinAnalyserButton) {
        button.addEventListener('click', onButtonAnalyser);
    }



    /**
     * Change l'url du navigateur sur localhost/accueil
     * 
     * @memberof Accueil
     * @returns {void}
     */
    function onButtonAccueil() {
        window.location.assign(loc+"/accueil");
    }

    let joinAccueilButton = document.getElementsByClassName('joinAccueil');

    for (let button of joinAccueilButton) {
        button.addEventListener('click', onButtonAccueil);
    }

})();